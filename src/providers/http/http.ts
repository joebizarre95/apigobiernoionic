import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
/*
  Generated class for the HttpProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HttpProvider {

  constructor(public http: HttpClient , public toast : ToastController , ) {
   
  }




  login(headers, url, data){
  	return new Promise( resolve => {
  		this.http.post(url, data, headers)
  			.subscribe(data => {
  				resolve(data)
  			}, err => {
  				console.log(err);

  				 let toast = this.toast.create({
			            message : 'Comprueba tu informacion porfavor ',
			            position: 'middle',
			            duration: 2000,
			          });
			          toast.present();    
			       
  			})

  	})
  		
  }

  getuserdata(url, headers){
    return new Promise( resolve => {
      this.http.get(url, headers)
        .subscribe(data => {
          resolve(data)
        }, err => {
          console.log(err);

          /* let toast = this.toast.create({
                  message : 'hubo un error',
                  position: 'middle',
                  duration: 4000,
                });
                toast.present();    
             */
        })

    })
      
  }

  log_out(headers, url, data){
  	return new Promise( resolve => {
  		this.http.post(url, data, headers)
  			.subscribe(data => {
  				resolve(data)
  			}, err => {
  				
  				 let toast = this.toast.create({
			            message : 'Comprueba tu conexion de red ',
			            position: 'middle',
			            duration: 2000,
			          });
  				 
			          toast.present();    
			       
  			})
  	})
  }


  set_data(headers, url, data){
    return new Promise( resolve => {
      this.http.post(url, data, headers)
        .subscribe(data => {
          resolve(data)
        }, err => {
            console.log(err);
           let toast = this.toast.create({
                  message : 'ups hubo un error ',
                  position: 'middle',
                  duration: 2000,
                });
                toast.present();    
        });
    });

  }



}
