import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LecheriaPage } from '../lecheria/lecheria';
import { App } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
/**
 * Generated class for the Save2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-save2',
  templateUrl: 'save2.html',
})
export class Save2Page {
id: any;

  data = {
    fase: null,
    inventario_semestre_anterior_b11: null,
    inventario_actual_a12: null, 
    diferencia:null,
    consumo_estimado_concentrado: null,
    ventas_contegral:null,
    ventas_finca:null,
    ventas_gec: null, 
    sales: null, 
    consumo_estimado_sale: null,


  }
  constructor( public app: App, public navCtrl: NavController, public navParams: NavParams , public http : HttpProvider) {
    this.id = navParams.data
    
  }
  datos = {}
  ionViewDidLoad() {
   

         let headers = {
         "Accept" :'application/json' ,
         'Content-Type': 'application/x-www-form-urlencoded',
      };

     
      let url = 'http://apiinnova.pythonanywhere.com/api/get_especific';

      let data = {
        pk : this.id,
      }

      this.http.log_out(headers, url, data)
        .then((success)=>{
         
           
         this.datos = success
         this.data.fase = this.datos[0]['fase']
         this.data.inventario_semestre_anterior_b11 = this.datos[0]['inventario_semestre_anterior_b11']
         this.data.inventario_actual_a12 = this.datos[0]['inventario_actual_a12']
         this.data.diferencia = this.datos[0]['diferencia']
         this.data.consumo_estimado_concentrado = this.datos[0]['consumo_estimado_concentrado']
         this.data.ventas_contegral = this.datos[0]['ventas_contegral']
         this.data.ventas_finca = this.datos[0]['ventas_finca']
         this.data.ventas_gec = this.datos[0]['ventas_gec']
         this.data.sales = this.datos[0]['sales']
         this.data.consumo_estimado_sale = this.datos[0]['consumo_estimado_sale']


        }, err => {
          console.log(err);
        })

  }

  volver(){
  	this.app.getRootNav().setRoot(LecheriaPage);
  }



}
