import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Guardar2Page } from '../guardar2/guardar2';

/**
 * Generated class for the Guardar1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-guardar1',
  templateUrl: 'guardar1.html',
})
export class Guardar1Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Guardar1Page');
  }

  data = {
    stc : null,
    atf : null,
    nombre_finca : null,
    razon_social : null,
    productor : null,
    correo_electronico : null,
    telefono_contacto: null,
    canal:null
    
  }

  save(){
  	
  	this.navCtrl.push(Guardar2Page , {
  		data : this.data
  	})
  }

  volver(){
  	this.navCtrl.pop();
  }



}
