import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Guardar1Page } from './guardar1';

@NgModule({
  declarations: [
    Guardar1Page,
  ],
  imports: [
    IonicPageModule.forChild(Guardar1Page),
  ],
})
export class Guardar1PageModule {}
