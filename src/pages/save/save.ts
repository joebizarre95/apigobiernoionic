import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Save1Page } from '../save1/save1';
import { Save2Page } from '../save2/save2';
import { Save3Page } from '../save3/save3';
import { HttpProvider } from '../../providers/http/http';




/**
 * Generated class for the SavePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-save',
  templateUrl: 'save.html',
   template: `
    <ion-tabs>
      <ion-tab tabIcon="map" [root]="tab1" tabTitle="ubicacion" [rootParams]="id"></ion-tab>
      <ion-tab tabIcon="home" [root]="tab2" tabTitle="predio" [rootParams]="id"></ion-tab>
      <ion-tab tabIcon="cash" [root]="tab3" tabTitle="comercial" [rootParams]="id"></ion-tab>
    </ion-tabs>`
})
export class SavePage {


	tab1: any;
	tab2: any;
	tab3: any;
  tab4: any;
  data = {};
  id: any;

  constructor(public navCtrl: NavController, public navParams: NavParams , public http : HttpProvider) {

  	this.tab1 = Save3Page;
  	this.tab2 = Save1Page;
  	this.tab3 = Save2Page;
    this.id = navParams.get('id');
    console.log(this.id);


  }

  ionViewDidLoad(){


    let headers = {
         "Accept" :'application/json' ,
         'Content-Type': 'application/x-www-form-urlencoded',
      };

     
      let url = 'http://apiinnova.pythonanywhere.com/api/get_especific';

      let data = {
        pk : this.id,
      }

      this.http.log_out(headers, url, data)
        .then((success)=>{
          console.log(success)
          this.data = success
        }, err => {
          console.log(err);
        })
  }



}
