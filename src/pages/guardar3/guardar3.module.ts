import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Guardar3Page } from './guardar3';

@NgModule({
  declarations: [
    Guardar3Page,
  ],
  imports: [
    IonicPageModule.forChild(Guardar3Page),
  ],
})
export class Guardar3PageModule {}
