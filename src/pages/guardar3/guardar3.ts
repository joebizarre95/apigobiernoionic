import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpProvider } from '../../providers/http/http';
import { AlertController } from 'ionic-angular';
import { LecheriaPage } from '../lecheria/lecheria';

/**
 * Generated class for the Guardar3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-guardar3',
  templateUrl: 'guardar3.html',
})
export class Guardar3Page {
 	guardar1 : any 
 	guardar2 : any
  constructor(public navCtrl: NavController, public navParams: NavParams, public geolocation : Geolocation , public http:HttpProvider , public alertCtrl: AlertController) {
  	this.guardar1 = this.navParams.get('guardar1')
  	this.guardar2 = this.navParams.get('guardar2')
  }


  data2 = {
    zona : null,
    municipio : null,
    vereda: null,
    latitud: 0,
    longitud: 0,
    altitud: 0,
    departamento : null,
  }

  ionViewDidLoad() {
  

  this.geolocation.getCurrentPosition().then((resp) => { 

     this.data2.latitud = resp.coords.latitude
      this.data2.longitud = resp.coords.longitude
      this.data2.altitud = resp.coords.altitude

  });

  }



  

  volver(){
  	this.navCtrl.pop()
  }

  save(){



    let guardar = {

      productor_id :1 ,
      stc : this.guardar1['stc'],
      atf : this.guardar1['atf'],
      razon_social : this.guardar1['razon_social'],
      canal : this.guardar1['canal'],
      nombre_finca : this.guardar1['nombre_finca'],
      productor : this.guardar1['productor'],
      vereda : this.data2.vereda ,
      municipio: this.data2.municipio,
      departamento : this.data2.departamento,
      latitud : this.data2.latitud,
      longitud: this.data2.longitud,
      altitud: this.data2.altitud, 
      fase: this.guardar2['fase'],
      inventario_semestre_anterior_b11 : this.guardar2['inventario_semestre_anterior_b11'],
      inventario_actual_a12 : this.guardar2['inventario_actual_a12'],
      diferencia: this.guardar2['diferencia'],
      consumo_estimado_concentrado: this.guardar2['consumo_estimado_concentrado'],
      ventas_contegral: this.guardar2['ventas_contegral'],
      ventas_finca : this.guardar2['ventas_finca'],
      ventas_gec : this.guardar2['ventas_gec'],
      marca_concentrado : this.guardar2['marca_concentrado'],
      categoria : this.guardar2['categoria'],
      consumo_estimado_sales: this.guardar2['consumo_estimado_sale'],
      marca_sales: this.guardar2['sales'],
      raza: this.guardar2['raza'],
      a_quien_vende_leche : this.guardar2['a_quien_vende_leche'],
      correo_electronico: this.guardar1['correo_electronico'],
      telefono_contacto: this.guardar1['telefono_contacto'],

    }



      let headers = {
         "Accept" :'application/json' ,
         'Content-Type': 'application/x-www-form-urlencoded',
      };

     
      let url = 'http://apiinnova.pythonanywhere.com/api/save_censos';


      this.http.set_data(headers, url, guardar)
        .then((success)=>{
          
          console.log(success)
              const confirm = this.alertCtrl.create({
                  title: 'PERFECTO!',
                  message: 'tu registro ha sido guardado sin ningun problema',
                  buttons: [
                    {
                      text: 'Ok!',
                      handler: () => {
                         this.navCtrl.setRoot(LecheriaPage);
                      }
                    }
                  ]
                });
                confirm.present();
                       


        }, err => {
          console.log(err);
        })

       

  }

}
