import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { LecheriaPage } from '../lecheria/lecheria';
import { LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ConfiguracionPage } from '../configuracion/configuracion';

/**
 * Generated class for the SelectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-selection',
  templateUrl: 'selection.html',
})
export class SelectionPage {
  data = {};
  token : any
  constructor(public navCtrl: NavController, public navParams: NavParams , public http :  HttpProvider ,  public loading : LoadingController, ) {
   this.token =  this.navParams.get('token');

  }

  ionViewDidLoad() {

    let url = 'http://127.0.0.1:8000/api/loginAPI/user/';

      let headers = {
         "Accept" :'application/json' ,
         'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'token ' + this.token['key']
      };

      this.http.getuserdata(url, headers)
        .then((succes)=>{
          console.log(succes)
        }, err=>{
          console.log(err)
        })


    
  }


  log_out(){

  	this.navCtrl.setRoot(HomePage);

  	
  	let url = 'http://apiinnova.pythonanywhere.com/api/loginAPI/logout';

  	let headers = {
         "Accept" :'application/json' ,
         'Content-Type': 'application/x-www-form-urlencoded',
      };

     let data = {}

      this.http.log_out(headers, url, data)
      	.then((success)=>{
      		console.log(success)
      		this.navCtrl.pop()
      	}, err => {
      		console.log(err)
      	});
  }

  lecheria(){

  	this.navCtrl.setRoot(LecheriaPage , {
      token : this.token['key']
    });

  	const loader = this.loading.create({
      content: "Cargando..",
      duration: 3000
    });

    loader.present();

  }

  configuracion(){
    this.navCtrl.push(ConfiguracionPage)
  }

}
