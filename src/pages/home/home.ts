import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { SelectionPage } from '../selection/selection';
import { LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  constructor(public navCtrl: NavController,
                 public navParams: NavParams , 
                      public http: HttpProvider , 
                        public loading : LoadingController , 
                          public toast : ToastController) {
  }

  data = {
  	username : null,
  	password : null,
  }

  ionViewDidLoad() {
    

  }

  login(){


  		let headers = {
         "Accept" :'application/json' ,
         'Content-Type': 'application/x-www-form-urlencoded',

      };
      
      let url = 'http://apiinnova.pythonanywhere.com/api/loginAPI/login/';
  		this.http.login(headers, url, this.data)
        .then((success)=>{

          console.log(success)
          let toast = this.toast.create({
            message : 'Cargano datos ',
            position: 'middle',
            duration: 2000,
          });

          toast.present();  

          this.navCtrl.push(SelectionPage, {
            token : success
          });

        }, err => {

          console.log(err)

        });
  }

}
