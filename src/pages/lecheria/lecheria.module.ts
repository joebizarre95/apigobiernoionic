import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LecheriaPage } from './lecheria';

@NgModule({
  declarations: [
    LecheriaPage,
  ],
  imports: [
    IonicPageModule.forChild(LecheriaPage),
  ],
})
export class LecheriaPageModule {}
