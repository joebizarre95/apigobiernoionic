import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SavePage } from '../save/save';
import { MapsPage } from '../maps/maps';
import { HttpProvider } from '../../providers/http/http';
import { ConfiguracionPage } from '../configuracion/configuracion';
import { HomePage } from '../home/home';
import { Guardar1Page } from '../guardar1/guardar1';
/**
 * Generated class for the LecheriaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lecheria',
  templateUrl: 'lecheria.html',
})
export class LecheriaPage {
  token = {}
  total_visitas : any;
  data : {};
  constructor(public navCtrl: NavController, public navParams: NavParams , public http: HttpProvider) {
   this.token = this.navParams.get('token');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LecheriaPage');
    console.log(this.token);

    let headers = {
         "Accept" :'application/json' ,
         'Content-Type': 'application/x-www-form-urlencoded',
      };

    let url = 'http://apiinnova.pythonanywhere.com/api/get_censos'

    this.http.getuserdata(url, headers)
      .then((success)=>{
        console.log(success);
        this.total_visitas = Object.keys(success).length
        this.data = success
      }, err =>{
        console.log(err);
      })
  }

  volver(){
  	this.navCtrl.setRoot(HomePage);
  }

  save(){
  	this.navCtrl.push(Guardar1Page);
  }

  maps(){
    this.navCtrl.push(MapsPage);
  }

  ver_mas(id){
    console.log(id)
    this.navCtrl.setRoot(SavePage, {
      id: id
    });
  }

   configuracion(){
    this.navCtrl.push(ConfiguracionPage)
  }

}
