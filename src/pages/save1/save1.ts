import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LecheriaPage } from '../lecheria/lecheria';
import { App } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';

import { Save2Page } from '../save2/save2';
import { Save3Page } from '../save3/save3';
/**
 * Generated class for the Save1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-save1',
  templateUrl: 'save1.html',
})
export class Save1Page {
    tab1 : any ;
    tab2: any; 
    tab3: any;
    id: any;
  constructor(public app : App, public navCtrl: NavController, public navParams: NavParams, public http : HttpProvider) {

  this.id = navParams.data
  
    this.tab1 = Save3Page;
    this.tab3 = Save2Page;

  }
  datos = {}
  stc : any
  atf: any 
  nombre_finca: any 
  razon_social : any 
  productor: any
  correo_electronico: any
  telefono_contacto: any 
  canal: any

  data = {
    stc : null,
    atf : null,
    nombre_finca : null,
    razon_social : null,
    productor : null,
    correo_electronico : null,
    telefono_contacto: null,
    canal:null
    
  }


  ionViewDidLoad() {

   
  

             let headers = {
         "Accept" :'application/json' ,
         'Content-Type': 'application/x-www-form-urlencoded',
      };

     
      let url = 'http://apiinnova.pythonanywhere.com/api/get_especific';

      let data = {
        pk : this.id,
      }

      this.http.log_out(headers, url, data)
        .then((success)=>{
          this.datos = success
          console.log(this.datos)

           this.data.stc = this.datos[0]['stc']
        this.data.atf = this.datos[0]['atf']
         this.data.nombre_finca = this.datos[0]['nombre_finca']
          this.data.razon_social = this.datos[0]['razon_social']
           this.data.productor = this.datos[0]['productor']
              this.data.correo_electronico = this.datos[0]['correo_electronico']
               this.data.correo_electronico = this.datos[0]['correo_electronico']
                this.data.telefono_contacto = this.datos[0]['telefono_contacto']
                   this.data.canal = this.datos[0]['canal']

        }, err => {
          console.log(err);
        })

      

  }


 

    volver(){
  	  this.app.getRootNav().setRoot(LecheriaPage);
  }





}
