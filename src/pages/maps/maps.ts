import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpProvider } from '../../providers/http/http';
/**
 * Generated class for the MapsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;
@IonicPage()
@Component({
  selector: 'page-maps',
  templateUrl: 'maps.html',
})

export class MapsPage {
@ViewChild('map') mapElement: ElementRef;
map: any;
lat:any;
long:any;
datos:any;
data : any;
  start = 'chicago, il';
  end = 'chicago, il';
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  constructor(public navCtrl: NavController, public navParams: NavParams , public geolocation : Geolocation , public http: HttpProvider) {
  }

  ionViewDidLoad() {
     this.findMe();
  }

    findMe(){
    this.geolocation.getCurrentPosition().then((position) => {
      let latLng = new google.maps.LatLng(4.0000000, -72.0000000);

         let mapOptions = {
           center: latLng,
           zoom: 7,
           mapTypeId: google.maps.MapTypeId.ROADMAP,
         }
         this.datos = new google.maps.LatLng( 4.0000000 , -72.0000000)
         this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
         this.addUser(latLng);
    }, (err) => { this.loadMap(); } );

  }


    loadMap(){
   console.log('loadmap');
  }

   addUser(position){


     
                     let latLng = new google.maps.LatLng(4.0000000, -72.0000000);
                     let infowindows = new google.maps.InfoWindow({
                       content: 'ubicacion'
                     });

                      var image = 'http://maps.google.com/mapfiles/ms/micons/info_circle.png';

                      let marker = new google.maps.Marker({
                          position: latLng,
                          map: this.map,
                          title: 'Evento',
                          mapTypeId: google.maps.MapTypeId.ROADMAP,
                          icon: image
                      })

                      marker.addListener('click', function() {
                            infowindows.open(this.map, marker);
                          });


                      let headers = {
                               "Accept" :'application/json' ,
                               'Content-Type': 'application/x-www-form-urlencoded',
                            };

                          let url = 'http://apiinnova.pythonanywhere.com/api/get_censos'
                          this.http.getuserdata(url, headers)
                            .then((success)=>{
                              this.data = success
                              for(var item of this.data ){
                                  console.log(item);

                                  let latLng = new google.maps.LatLng(item['latitud'] , item['longitud']);

                                  let infoWindows = new google.maps.InfoWindow({
                                    content: item['nombre_finca']
                                  });

                                  var image = 'http://maps.google.com/mapfiles/ms/micons/info_circle.png';

                                   let marker = new google.maps.Marker({
                                  position: latLng,
                                  map: this.map,
                                  title: 'Evento',
                                  mapTypeId: google.maps.MapTypeId.ROADMAP,
                                  icon: image
                              })

                              marker.addListener('click', function() {
                                    infoWindows.open(this.map, marker);
                                  });

                              }
                            }, err =>{
                              console.log(err);
                            })

              

  }

   calcularRuta(){



  }



}
