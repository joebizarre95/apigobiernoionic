import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Guardar3Page } from '../guardar3/guardar3';

/**
 * Generated class for the Guardar2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-guardar2',
  templateUrl: 'guardar2.html',
})
export class Guardar2Page {
	guardar1 : any
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.guardar1 = this.navParams.get('data');
  }

  data = {
    fase: null,
    inventario_semestre_anterior_b11: null,
    inventario_actual_a12: null, 
    diferencia:null,
    consumo_estimado_concentrado: null,
    ventas_contegral:null,
    ventas_finca:null,
    ventas_gec: null, 
    sales: null, 
    consumo_estimado_sale: null,
    marca_concentrado: null,
    categoria: null,
    raza: null,
    a_quien_vende_leche: null,

  }

  ionViewDidLoad() {
  

  }

  save(){
  	this.navCtrl.push(Guardar3Page, {
  			guardar1 : this.guardar1,
  			guardar2 : this.data 
  				})
  }

  volver(){
    this.navCtrl.pop();
  }

}
