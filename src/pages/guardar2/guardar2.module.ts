import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Guardar2Page } from './guardar2';

@NgModule({
  declarations: [
    Guardar2Page,
  ],
  imports: [
    IonicPageModule.forChild(Guardar2Page),
  ],
})
export class Guardar2PageModule {}
