import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LecheriaPage } from '../lecheria/lecheria';
import { App } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpProvider } from '../../providers/http/http';
/**W
 * Generated class for the Save3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-save3',
  templateUrl: 'save3.html',
})
export class Save3Page {
  lat : any;
  long: any;
  alt: any;
  data = {};
  id: any;

  constructor(public app : App, public navCtrl: NavController, public navParams: NavParams , public geolocation : Geolocation , public http: HttpProvider) {
    this.id = navParams.data
   
  }


  data2 = {
    zona : null,
    municipio : null,
    vereda: null,
    latitud: 0,
    longitud: 0,
    altitud: 0,
  }

  ionViewDidLoad() {
    
    this.geolocation.getCurrentPosition().then((resp) => {
      
      
      
      }).catch((error) => {
        console.log('Error getting location', error);
      });

        let headers = {
         "Accept" :'application/json' ,
         'Content-Type': 'application/x-www-form-urlencoded',
      };

     
      let url = 'http://apiinnova.pythonanywhere.com/api/get_especific';

      let data = {
        pk : this.id,
      }

      this.http.log_out(headers, url, data)
        .then((success)=>{


         
          this.data = success

          this.data2.latitud = this.data[0]['latitude']
          this.data2.longitud = this.data[0]['longitude']
          this.data2.altitud = this.data[0]['altitude']
          this.data2.zona = 'ANTIOQUIA'
          this.data2.municipio = this.data[0]['municipio']
          this.data2.vereda = this.data[0]['vereda']
          
        }, err => {
          console.log(err);
        })


  }


    volver(){
  	  this.app.getRootNav().setRoot(LecheriaPage);
  }

  save(){
    console.log(this.data);
  }



}
