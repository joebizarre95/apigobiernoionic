import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { HomePageModule } from '../pages/home/home.module';
import { HttpProvider } from '../providers/http/http';
import { HttpClientModule } from '@angular/common/http';
import { SelectionPageModule } from '../pages/selection/selection.module';
import { LecheriaPageModule } from '../pages/lecheria/lecheria.module';
import { SavePageModule } from '../pages/save/save.module';
import { Save1PageModule } from '../pages/save1/save1.module';
import { Save2PageModule } from '../pages/save2/save2.module';
import { Save3PageModule } from '../pages/save3/save3.module';
import { MapsPageModule } from '../pages/maps/maps.module';
import { ConfiguracionPageModule } from '../pages/configuracion/configuracion.module';
import { Guardar1PageModule } from '../pages/guardar1/guardar1.module';
import { Guardar2PageModule } from '../pages/guardar2/guardar2.module';
import { Guardar3PageModule } from '../pages/guardar3/guardar3.module';


import { Geolocation } from '@ionic-native/geolocation';


@NgModule({
  declarations: [
    MyApp,
    
  ],
  imports: [
    BrowserModule,
    HomePageModule,
    HttpClientModule,
    SelectionPageModule,
    LecheriaPageModule,
    SavePageModule,
    Save1PageModule,
    Save2PageModule,
    Save3PageModule,
    MapsPageModule,
    ConfiguracionPageModule,
    Guardar1PageModule,
    Guardar2PageModule,
    Guardar3PageModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,

    
  ],
  providers: [
    StatusBar,
    SplashScreen,

    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpProvider,
    Geolocation,

  ]
})
export class AppModule {}
